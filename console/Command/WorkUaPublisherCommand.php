<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Parser\WorkUa\WorkUaCollector;

/**
* WorkUaPublisherCommand
*/
class WorkUaPublisherCommand extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName("work-ua:publisher")
            ->setDescription("Command work-ua:publisher")
            ->addOption(
                'url',
                null,
                InputOption::VALUE_REQUIRED,
                'start parsing url',
                'https://www.work.ua/resumes-kyiv-sales/?photo=1'
            );
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $factory = new WorkUaCollector();
        $factory::$urlFilter = $input->getOption('url');
        var_dump( $factory::$urlFilter);
        $factory->collect();
    }
}