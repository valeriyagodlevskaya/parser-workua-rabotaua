<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Parser\RabotaUa\RabotaUaCollector;

/**
* WorkUaPublisherCommand
*/
class RabotaUaPublisherCommand extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName("rabota-ua:publisher")
            ->setDescription("Command rabota-ua:publisher")
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $factory = new RabotaUaCollector();
        $factory->collect();
    }
}