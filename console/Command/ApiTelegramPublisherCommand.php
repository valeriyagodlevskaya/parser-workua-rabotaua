<?php

namespace Command;

use Api\Telegram\ApiTelegram;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Parser\WorkUa\WorkUaCollector;

/**
* ApiTelegramPublisherCommand
*/
class ApiTelegramPublisherCommand extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName("api-t:publisher")
            ->setDescription("Command api-t:publisher")
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $factory = new ApiTelegram();
        $factory->collect();
    }
}