#!/usr/bin/env php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
// if you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
umask(0000);
//set_time_limit(0);
require __DIR__.'/vendor/autoload.php';

use Console\ApplicationExtra;

$application = new ApplicationExtra(__DIR__.'/console/Command/');
$application->run();