<?php
/**
 * Created by PhpStorm.
 * User: lera
 * Date: 16.01.18
 * Time: 13:34
 */

namespace Parser\WorkUa;

use Parser\CollectorFactory;
use Parser\Helpers\Parser;
use Sunra\PhpSimple\HtmlDomParser;
use GuzzleHttp;
use GuzzleHttp\Cookie\CookieJar;
use Parser\Helpers\DataBaseModel;
use Parser\Helpers\UserModel;

/**
 * Class WorkUaCollector
 * @package Parser\WorkUa
 * @property  $this->driver \RemoteWebDriver
 */
class WorkUaCollector extends CollectorFactory
{

    const QUEUE = 'work-ua';
    const ROUTING_KEY = 'work-ua';
    const DS_ORIGIN_ID = 4408;

    const LOGIN_USERNAME = 'pregochain@gmail.com';
    const LOGIN_PASS = 'pregoshop1';
    const FILE_NAME = __DIR__.'/files/firefox/cookie.txt';

    protected static $cookieJar;
    public static $consumerCallback = 'parseMessage';
    public $cookieBrowser;

    public $driver;

    public static $urlFilter = 'https://www.work.ua/resumes-sales/?photo=1';

    public $month = [
        'января'=> '01',
        'февраля' => '02',
        'марта' => '03',
        'апреля' => '04',
        'мая' => '05',
        'июля' => '06',
        'июня' => '07',
        'августа' => '08',
        'сентября' => '09',
        'октября' => '10',
        'ноября' => '11',
        'декабря' => '12'
    ];

    public $parseFilters = [
        '~.*/multimedia/feeds/facet/.*\.txt~'
    ];

    public $scanFilters = [
        '~https://www.work.ua/resumes-kyiv-sales/?photo=1/$~'
    ];

    public $startUrl = 'https://www.work.ua/resumes-kyiv-sales/?photo=1';
    public $urlLogin = 'https://www.work.ua/employer/login/';

    public $getCookie;

    public function collect()
    {
        try {
            for($i=1; $i<11; $i++) {
                $this->parsePage($i, $this->startUrl);
            }
        } catch (\Throwable  $t){
           // var_dump($t->getMessage());die();
            $this->getSeleniumLogin($this->urlLogin);

    }
        if(!$this->driver) {
            $desired_capabilities = \DesiredCapabilities::firefox();
            //$desired_capabilities->setCapability(\WebDriverCapabilityType::NATIVE_EVENTS, true);
            //$desired_capabilities->setCapability('pageLoadStrategy', 'eager');
            $this->driver = \RemoteWebDriver::create(SELENIUM_HOST, $desired_capabilities);
        }
    }

    public function consume()
    {

    }


    /**
     * @param $url
     */
    public function getSeleniumLogin($url)
    {
        $this->driver->get($url);
        sleep(2);
        $this->driver->findElement(\WebDriverBy::name('login[1]'))->sendKeys(self::LOGIN_USERNAME);
        sleep(2);
        $this->driver->findElement(\WebDriverBy::name('password[1]'))->sendKeys(self::LOGIN_PASS);
        sleep(2);
        $this->driver->findElement( \WebDriverBy::className('btn-block'))->click();
        sleep(5);

        $cookieArray = $this->driver->manage()->getCookies();
        $cookiesArr = array_column($cookieArray, 'value', 'name');
        $this->cookieBrowser = CookieJar::fromArray($cookiesArr, 'work.ua');
        $cookString = serialize($this->cookieBrowser);
        file_put_contents(self::FILE_NAME, $cookString);

    }

//    public function login()
//    {
//        $url = 'https://www.work.ua/employer/login/';
//        static::setCookieJar();
//       $content =  Parser::getUrl($url, static::$cookieJar);
//       $html = HtmlDomParser::str_get_html($content);
//       $secret = $html->find('input[name="secret"]');
//       $secret = $secret[0]->value;
//        $params = [
//            'login' => ['1' => self::LOGIN_USERNAME],
//            'password' => ['1' => self::LOGIN_PASS],
//            'secret' => $secret,
//            'remember' => 'on'
//        ];
//        $client = new GuzzleHttp\Client();
//        $res = $client->request('POST', $url, [
//            'headers' => [
//                'User-Agent' => Parser::$userAgent,
//                'Accept' => 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8',
//                'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
//                'Cache-Control' =>	'max-age=0',
//                'Connection' => 'keep-alive',
//                'Cookie' => '_dy_soct=65526.110688.15119498…78049*89552.121107.1512378049',
//                'Upgrade-Insecure-Requests' => '1',
//            ],
//            'form_params'=>$params,
//            'cookies' => static::$cookieJar,
//        ]);
//        //echo $res->getBody();
//
//    }
    public static function setCookieJar()
    {
        if (!static::$cookieJar) {
            static::$cookieJar = new CookieJar();
        }
    }

    /**
     * @param $pageNumber
     */
    public function parsePage($pageNumber, $url)
    {
        $this->driver->get($url.'&page='.$pageNumber);
        die();
        $content = $this->driver->getPageSource();

        //$pageHtml = Parser::getSeleniumUrl($urlStart);
       // $content =  Parser::getUrl($urlStart, static::$cookieJar);
        $html = HtmlDomParser::str_get_html($content);
        //$resumeLinks = $html->find('h2>a');
        $pattern = '/\D/';
        $linkUser = $html->find('div[class="photo-holder"] a');
        foreach ($linkUser as $item) {
            $userLink = $item->href;
            $idUser = preg_replace($pattern, '', $userLink);
            sleep(5);
            if (!empty($idUser)) {
                $dataUser = $this->getUserData($idUser);

                echo $dataUser;
            }
        }

    }

    /**
     * @param $id
     * Парсим страницу с резюме, телефон, email
     */
    public function getUserData($id)
    {
        $this->getCookie = file_get_contents(self::FILE_NAME);
        $this->getCookie = unserialize($this->getCookie);

        $user = new UserModel();
        sleep(5);
        $this->getListUser($id, $user);

        $ajaxLink = 'https://www.work.ua/_data/_ajax/resumes_selection.php';
        $post_data = [
            'func' => 'showResumeContacts',
            'id' => $id
        ];

        $client = new GuzzleHttp\Client();
        $response = $client->request('POST', $ajaxLink, [
            'headers' => [
                'User-Agent' => Parser::$userAgent,
                'Accept' => 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8',
                'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                'Cache-Control' =>	'max-age=0',
                'Connection' => 'keep-alive',
                'Upgrade-Insecure-Requests' => '1',
            ],
            'form_params' =>  $post_data,
//            'cookies' => $this->cookieBrowser,
             'cookies' => $this->getCookie
        ]);
        $stdObject = GuzzleHttp\json_decode($response->getBody());
        $objectData = $stdObject->contact;

        $user->external = $id;
        $formatPhone = preg_replace('~[^\s\d]*~', '', $objectData->phone_prim);
        $testPhone = preg_replace('~(\d{1,4})\s~', '$1', $formatPhone);
        $t = strpos($testPhone, '0');
        $test = substr($testPhone, $t,10);
        $user->phone = $test;
        $user->phones = $objectData->phone_prim;
        $user->email = $objectData->email;
        $user->save('workua');

    }

    /**
     * @param $id
     * @param $user
     */
    public function getListUser($id, $user)
    {
        $urlResume = 'https://www.work.ua/resumes/'.$id;
        $client = new GuzzleHttp\Client();
        $dataUser = $client->request('GET', $urlResume, [
            'cookies' => static::$cookieJar
        ]);
        $user->url = $urlResume;
        $content = $dataUser->getBody();
        $contentObject = (string) $content;
        $html = HtmlDomParser::str_get_html($contentObject);

        //краткое описание соискателя
        $description = $html->find('meta[name="Description"]');
        $user->description = $description[0]->content;

        //дата последнего обновления
        $findWord = '~Резюме от&nbsp;(.*)<~uU';
        $find = preg_match_all($findWord, $content, $matches);
        $userUpdate = trim($matches[1][0]);
        $dateFormat = explode(' ', $userUpdate);
        $findMonth = str_replace($dateFormat[1], $this->month[$dateFormat[1]],$userUpdate);
        $user->date = str_replace(' ', '/', $findMonth);
        //$user->date = $userUpdate;

        //фото
        $findLinkImage = $html->find('p.add-top img');
        $linkImage = $findLinkImage[0]->attr["src"];
        $user->photo = 'https:'.$linkImage;
       $contentImage = file_get_contents('https:'.$linkImage);
       file_put_contents(__DIR__.'/files/'.$id.'.jpg', $contentImage);

       //ФИО
        $findName = $html->find('h1.cut-top');
        $user->name = $findName[0]->text();

        //Дата рождения
        $findData = $html->find('dl dd');
        $dateBirthday = $findData[0]->text();
        $userDateBirthday = preg_match_all(
            '~(\d.*)\s\((\d+)&~uU',
                    $dateBirthday,
                    $result
        );

        $formatDateBirthday = explode(' ', $result[1][0]);
        $user->dayBirth = $formatDateBirthday[0];
        $user->monthBirth = $formatDateBirthday[1];
        $user->yerBirth = $formatDateBirthday[2];
        $user->dateBirthday = $result[1][0];
        $user->age = $result[2][0];
        $user->city = $findData[1]->text();
    }


}