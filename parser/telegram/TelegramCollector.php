<?php
namespace Parser\Telegram;

use Facebook\WebDriver\Interactions\Internal\WebDriverCoordinates;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteMouse;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Parser\Helpers\Parser;
use Facebook\WebDriver\Remote\RemoteWebDriver;


/**
 * Class TelegramCollector
 * @package Parser\Telegram
 * @property RemoteWebDriver $driver
 */
class TelegramCollector
{
    protected static $driver;

    /**
     * TelegramCollector constructor.
     */
    public function __construct()
    {
        if(!static::$driver) {
            $desired_capabilities = DesiredCapabilities::firefox();
            $desired_capabilities->setCapability(WebDriverCapabilityType::NATIVE_EVENTS, false);
            $desired_capabilities->setCapability('pageLoadStrategy', 'eager');
            static::$driver = RemoteWebDriver::create(SELENIUM_HOST, $desired_capabilities);
        }
    }


    public function login()
    {
        //открываем страницу
         static::$driver->get('https://web.telegram.org/#/login');
        sleep(10);
        //ожидаем ее загрузки
        //static::$driver->manage()->timeouts()->implicitlyWait(20);
        static::$driver->wait(20, 1000)->until(WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::name('phone_number')));

        $content =static::$driver->getPageSource();
       //var_dump($content);
       // $element = static::$driver->findElement(WebDriverBy::name('phone_number'));



       // static::$driver->wait('60', 3000);//->until(WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::name('phone_number')));
       // static::$driver->findElement(WebDriverBy::tagName("input"));

//        var_dump(static::$driver->getMouse());
//        $inputPhone = static::$driver->findElement(WebDriverBy::name('phone_number'));
//        $inputPhone->sendKeys('930148157');
        static::$driver->quit();

    }

    public function sendMassage()
    {
        Parser::getSeleniumUrl('https://web.telegram.org/#/login');

    }

}