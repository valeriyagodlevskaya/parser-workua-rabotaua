<?php

namespace Parser;


abstract class CollectorFactory
{
    public abstract function collect();

    public abstract function consume();
}