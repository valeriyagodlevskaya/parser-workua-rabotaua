<?php
/**
 * Created by PhpStorm.
 * User: lera
 * Date: 19.01.18
 * Time: 15:21
 */

namespace Parser\RabotaUa;

use Parser\Helpers\OriginModel;
use Parser\Helpers\UserModel;
use Parser\CollectorFactory;
use Parser\Helpers\Parser;
use Sunra\PhpSimple\HtmlDomParser;
use GuzzleHttp;
use GuzzleHttp\Cookie\CookieJar;
use Parser\Helpers\DataBaseModel;
use Exception;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class RabotaUaCollector
 * @package Parser\RabotaUa
 */

class RabotaUaCollector extends CollectorFactory
{
    protected static $cookieJar;

    public $month = [
        'янв' => 'января',
        'фев' => 'февраля',
        'мар' => 'марта',
        'апр' => 'апреля',
        'май' => 'мая',
        'июл' => 'июля',
        'июн' => 'июня',
        'авг' => 'августа',
        'сен' => 'сентября',
        'окт' => 'октября',
        'ноя' => 'ноября',
        'дек' => 'декабря',
        'сiч' => 'января',
        'лют' => 'февраля',
        'бер' => 'марта',
        'квi' => 'апреля',
        'тра' => 'мая',
        'чер' => 'июля',
        'лип' => 'июня',
        'сер' => 'августа',
        'вер' => 'сентября',
        'жов' => 'октября',
        'лис' => 'ноября',
        'гру' => 'декабря',
    ];

    public $urlFilter = 'https://rabota.ua/employer/find/cv_list?parentid=17&period=7&hasphoto=1&sort=date';
    public $log;

    public function collect()
    {
        $this->log = new Logger('rabota-log');
        $this->log->pushHandler(new StreamHandler(__DIR__.'/files/logs/rabota-log.log', Logger::WARNING));
        $this->login();
            for($i=1; $i<11; $i++) {
                $this->getListUser($i, $this->urlFilter);
            }

    }

    public function consume()
    {
        // TODO: Implement consume() method.
    }


    /**
     * method autorization
     */
    public function login()
    {
        $this->log->info('Start login');
        $url = 'https://rabota.ua/employer/login';
        static::setCookieJar();
        $content =  Parser::getUrl($url, static::$cookieJar);
        $this->log->info('Get content page');
        $html = HtmlDomParser::str_get_html($content);
        $viewstate = $html->find('input[name="__VIEWSTATE"]');
        $viewstate = $viewstate[0]->value;
        $params = [
            '__EVENTTARGET' => 'ctl00$content$ZoneLogin$btnLogin',
            '__EVENTARGUMENT' => '',
            '__VIEWSTATE' => $viewstate,
            '__VIEWSTATEGENERATOR' => 'BFE5824D',
            'ctl00$content$ZoneLogin$txLogin' => 'Dmdprego@gmail.com',
            'ctl00$content$ZoneLogin$txPassword' => 'skeev3y3',
            'ctl00$content$ZoneLogin$chBoxRemember' => 'on',
            'ctl00$Footer$ddlLanguage' => 2
        ];

        $client = new GuzzleHttp\Client();
        $res = $client->request("POST", $url, [
            'headers' => [
                'User-Agent' => Parser::$userAgent,
                'Accept' => 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8',
                'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                'Cache-Control' =>	'max-age=0',
                'Connection' => 'keep-alive',
                'Cookie' => '_dy_soct=65526.110688.15119498…78049*89552.121107.1512378049',
                'Upgrade-Insecure-Requests' => '1',
            ],
            'form_params' => $params,
            'cookies' => static::$cookieJar,

        ]);
        $this->log->info('End login');
        //echo $res->getBody();


    }

    public static function setCookieJar()
    {
        if (!static::$cookieJar) {
            static::$cookieJar = new CookieJar();
        }
    }

    /**
     * @param $pageNumber
     */
    public function getListUser($pageNumber, $urlFilter)
    {
        //$urlList = 'https://rabota.ua/%D1%80%D0%B5%D0%B7%D1%8E%D0%BC%D0%B5/%D0%B2_%D0%BF%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B0%D1%85/%D1%83%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0';
        $this->log->info('get list user');
        $urlList = $urlFilter.'&pg='.$pageNumber;
        $content =  Parser::getUrl($urlList, static::$cookieJar);
        $html = HtmlDomParser::str_get_html($content);
        $findLink = $html->find('h3[class="cv-list__cv-title"] a');
        $this->log->info('проходим по списку, получаем ссылки на cv');
        foreach ($findLink as $link) {
            $linkId = $link->href;
            sleep(5);
            try {
                $this->getDataUser($linkId);
            } catch (\Exception $e) {
                echo $e->getMessage(), '\n';
            }

        }


    }

    /**
     * @param $urlContact
     * @param $user
     * @return bool
     *
     *
     */
    public function getContactDataUser($urlContact, $user)
    {
        $this->log->info('получаем контактные данные каждого юзера');
        static::setCookieJar();

        $content =  Parser::getUrl($urlContact, static::$cookieJar);
        $html = HtmlDomParser::str_get_html($content);
        $viewstate = $html->find('input[name="__VIEWSTATE"]');
        $viewstate = $viewstate[0]->value;
        $viewstategenerator = $html->find('input[name="__VIEWSTATEGENERATOR"]');
        $viewstategenerator = $viewstategenerator[0]->value;
        $hdnPrev = $html->find('input[name="ctl00$centerZone$BriefResume1$CvView1$cvHeader$hdnPrev"]');
        $hdnPrev = $hdnPrev[0]->value;
        $params = [
            '__EVENTTARGET' => 'ctl00$centerZone$BriefResume1$CvView1$cvHeader$lnkBuyCv',
            '__EVENTARGUMENT' => '',
            '__VIEWSTATE' => $viewstate,
            '__VIEWSTATEGENERATOR' => $viewstategenerator,
            'ctl00$centerZone$BriefResume1$CvView1$cvHeader$AjaxLogin1$txtLogin' => '',
            'ctl00$centerZone$BriefResume1$CvView1$cvHeader$AjaxLogin1$txtPassword' => '',
            'ctl00$centerZone$BriefResume1$CvView1$cvHeader$hdnPrev' => $hdnPrev,

        ];

        $client = new GuzzleHttp\Client();
        $res = $client->request('POST', $urlContact, [
            'form_params' => $params,
            'cookies' => static::$cookieJar,
        ]);

        $pageContent = $res->getBody();
        $htmlContent = HtmlDomParser::str_get_html($pageContent);

        $email = $htmlContent->find('span[id="centerZone_BriefResume1_CvView1_cvHeader_lblEmailValue"]');
        $phone = $htmlContent->find('span[id="centerZone_BriefResume1_CvView1_cvHeader_lblPhoneValue"]');
        $fullDateBirth = $htmlContent->find('span[id="centerZone_BriefResume1_CvView1_cvHeader_lblBirthDateValue"]');
        $findCity = $htmlContent->find('span[id="centerZone_BriefResume1_CvView1_cvHeader_lblRegionValue"]');

        $this->log->info('get content page cv');

        if (!empty($email) || !empty($phone) || !empty($fullDateBirth) || !empty($findCity)) {
            $user->email = $email[0]->text();

            $formatPhone = preg_replace('~[^\s\d]*~', '', $phone[0]->text());
            $phoneUser = preg_replace('~(\d{1,4})\s~', '$1', $formatPhone);
            $position = strpos($phoneUser, '0');
            $user->phone = substr($phoneUser, $position,10);
            $user->phones = $phone[0]->text();
            $dateBirth = '';
            $age = '';
            foreach ($fullDateBirth as $date) {
                $dateBirthday = $date->text();
                preg_match_all(
                    '~([^\)]+)\((.*)\)~',
                    $dateBirthday,
                    $result
                );
                $dateBirth = $result[1];
                $age = $result[2];
            }
            $user->city = $findCity[0]->text();

            $arrayDate = explode(' ', $dateBirth[0]);
            var_dump($dateBirth[0]);
            $formatDate = str_replace($arrayDate[1], $this->month[$arrayDate[1]], $dateBirth[0]);
            $userDateBirth = explode(' ', $formatDate);
            var_dump($userDateBirth[0]);
            $user->dayBirth = $userDateBirth[0];
            $user->monthBirth = $userDateBirth[1];
            $user->yerBirth = $userDateBirth[2];

            $user->dateBirthday = $dateBirth[0];

            $ageUser = preg_replace('~\D+~','', $age[0]);
            $user->age = $ageUser;
        } else {
            $this->log->info('контактные данные закрыты');
            echo 'Контактные данные закрыты или отсутствуют: '.$urlContact, "\n";
            return false;

        }
        $this->log->info('save data');
        $user->save('rabotaua');

    }

    /**
     * @param $id
     * @throws Exception
     */
    public function getDataUser($id)
    {
        $this->log->info('getDataUser');
        $user = new UserModel();

        $url = 'https://rabota.ua'.$id;
        $user->url = $url;
        $idUser = preg_replace('~\D+~','', $id);
        $user->external = $idUser;
        $client = new GuzzleHttp\Client();
        $content = $client->request('GET', $url, [
            'cookies' => static::$cookieJar,
            'allow_redirects' => false
        ]);
        if ($content->getStatusCode() == 302) {
            throw new Exception('Not found');
        }

        $resContent = $content->getBody();
        $this->log->info('getBody');
        $htmlContent = HtmlDomParser::str_get_html($resContent);
        $findFullName = $htmlContent->find('span[id="centerZone_BriefResume1_CvView1_cvHeader_lblName"]');
        $user->name = $findFullName[0]->text();
        $findDescription = $htmlContent->find('meta[name="description"]');
        $user->description = $findDescription[0]->content;
        $dateUpdate = $htmlContent->find('div[class="rua-g-right"] span[class="muted"]');

        //last date update resume
        $lastDateUpdate = $dateUpdate[0]->text();
        $user->date = str_replace('резюме обновлено ' , '', $lastDateUpdate);

        $findImage = $htmlContent->find('div[class="photo"] img');
        //save url in DataBase
        $photo = $findImage[0]->attr["src"];
        $user->photo = $photo;

        //upload photo in directory
        $getPhoto = file_get_contents($photo);
        //file_put_contents(__DIR__.'/files'.$id.'.jpg', $getPhoto);
        $this->getContactDataUser($url, $user);
        $this->log->info('getContsctDataUser');
    }
}