<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 29.11.17
 * Time: 18:35
 */

namespace Parser\Helpers;

use GuzzleHttp\Client;


class OriginModel extends BaseOriginModel
{


    public static $batchModels = [];

    public $intid = null;
    public $h1;
    public $name;
    public $title_source;
    public $image;

    public $screenshot = null;
    public $description;
    public $url_source;
    public $price;
    public $currency = null;
    public $category = null;
    public $category_id = null;
    public $review_json = null;
    public $review_count = null;
    public $best_rating = null;
    public $rating_value = null;
    public $brand_id = null;
    public $brand_name = null;
    public $about_brand = null;
    public $brand_url = null;
    public $meta_keywords = null;
    public $metatitle = null;
    public $metadescription = null;
    public $text = null;
    public $date_posted = null;
    public $date_published = null;
    public $restrictions = null;
    public $ingredients = null;
    public $ingredient_list = null;
    public $instructions = null;
    public $screenshots = null;
    public $same_as = null;
    public $likes_count = null;
    public $shipping_info = null;
    public $author = null;


    private $required = ['h1', 'name', 'title_source', 'image', 'description', 'url_source', 'price'];

    private $errors = [];

    /**
     * Валидирует модель перед отправкой в хранилище
     * @return bool
     */
    public function validate()
    {
        foreach ($this->required as $attribute) {
            if(!isset($this->$attribute)) {
                $this->errors[] = "'$attribute' attribute must be set";
            }
        }
        return empty($this->errors);
    }


    public function getDataToSend()
    {
        $props = call_user_func('get_object_vars', $this);
        //$props = get_object_vars($this);
        $data = [];
        foreach ($props as $key=>$prop) {
            if(!$prop) {
                continue;
            }
            $data[$key] = $prop;
        }
        return $data;
    }
}