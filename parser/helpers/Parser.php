<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 29.11.17
 * Time: 12:48
 */

namespace Parser\Helpers;


use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * Class Parser
 * @property RemoteWebDriver $driver
 * @package Parser\Helpers
 */
class Parser
{
    const SELENIUM_PARSER = 'selenium';
    const GUZZLE_PARSER = 'guzzle';

    public static $userAgent = 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0';

    public $startUrl;
    public $parseFilters = [];
    public $scanFilters = [];
    private $scanned = [];

    public $baseUrl;

    public static $driver;

    /**
     * Возвращает содержимое по URL
     * @param string $url
     * @param CookieJar|null $cookieJar
     * @return string
     */
    public static function getUrl(string  $url, CookieJar &$cookieJar = null)
    {
        $client = new Client();

        if(!$cookieJar) {
            $cookieJar = new CookieJar();
        }

        try{
            $res = $client->request('GET', $url, [
                'headers' => [
                    'User-Agent' => static::$userAgent,
                    'Accept' => 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8',
                    'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                    'Cache-Control' =>	'max-age=0',
                    'Connection' => 'keep-alive',
                    'Cookie' => '_dy_soct=65526.110688.15119498…78049*89552.121107.1512378049',
                    'Upgrade-Insecure-Requests' => '1',
                ],
                'cookies' => $cookieJar,
            ]);
        } catch (ClientException $e) {
            echo $e->getMessage() , "\n";
            return '';
        }

        return (string)$res->getBody();
    }


    /**
     * @param string $url
     * @return string
     */
    public static function getSeleniumUrl(string $url)
    {
        if(!static::$driver) {
            $desired_capabilities = DesiredCapabilities::firefox();
            $desired_capabilities->setCapability(WebDriverCapabilityType::NATIVE_EVENTS, false);
            $desired_capabilities->setCapability('pageLoadStrategy', 'eager');
            static::$driver = RemoteWebDriver::create(SELENIUM_HOST, $desired_capabilities);
        }
        $content = static::$driver->get($url)->getPageSource();
        return $content;
    }

    /**
     * Преобразует относительный URL в полный
     * @param string $rel Относительный URL
     * @param string $base Базовый URL
     * @return string Полный URL
     */
    public static function rel2abs(string $rel, string $base) {
        if(strpos($rel,"//") === 0) {
            return "http:".$rel;
        }
        /* return if  already absolute URL */
        if  (parse_url($rel, PHP_URL_SCHEME) != '') return $rel;
        $first_char = substr ($rel, 0, 1);
        /* queries and  anchors */
        if ($first_char == '#'  || $first_char == '?') return $base.$rel;
        /* parse base URL  and convert to local variables:
        $scheme, $host,  $path */
        $path = '';
        extract(parse_url($base));
        /**
         * @var string $path
         * @var string $host
         * @var string $scheme
         */
        /* remove  non-directory element from path */
        $path = preg_replace('#/[^/]*$#',  '', $path);
        /* destroy path if  relative url points to root */
        if ($first_char ==  '/') $path = '';
        /* dirty absolute  URL */
        $abs =  "$host$path/$rel";
        /* replace '//' or  '/./' or '/foo/../' with '/' */
        $re =  array('#(/.?/)#', '#/(?!..)[^/]+/../#');
        for($n=1; $n>0;  $abs=preg_replace($re, '/', $abs, -1, $n)) {}
        /* absolute URL is  ready! */
        return  $scheme.'://'.$abs;
    }

    /**
     * Рекурсивно собирает все ссылки с сайта, если они подходят по фильтрам $this->scanFilters ,
     * и вызывает колбек для тех, что подходят под this->parseFilters
     * @param callable $callback
     * @param null|string $url
     * @param string $method
     */
    public function scan(callable $callback, $url = null, $method=self::GUZZLE_PARSER )
    {
        $url = $url?$url:$this->startUrl;

        $this->clearQueue();


        do{
            echo $url . "\n";
            $url = filter_var ($url, FILTER_SANITIZE_URL);
            if (!filter_var ($url, FILTER_VALIDATE_URL)) {
                return;
            }

            switch ($method) {
                case static::SELENIUM_PARSER:
                    $content = Parser::getSeleniumUrl($url);
                    break;
                case static::GUZZLE_PARSER:
                    $content = Parser::getUrl($url);
                    break;
            }

            if(!$content) {
                return;
            }

            $html = HtmlDomParser::str_get_html($content);
            try {
                if(!is_object($html)) {
                    throw new \Exception('$html is not ia object');
                }
                $a1 = $html->find('a');
                foreach ($a1 as $val) {
                    $next_url = $val->href or "";
                    $fragment_split = explode("#", $next_url);
                    $next_url = $fragment_split[0];
                    if ((substr($next_url, 0, 7) != "http://") &&
                        (substr($next_url, 0, 8) != "https://") &&
                        (substr($next_url, 0, 6) != "ftp://") &&
                        (substr($next_url, 0, 7) != "mailto:")) {
                        $next_url = @Parser::rel2abs($next_url, $url);
                    }
                    $next_url = filter_var($next_url, FILTER_SANITIZE_URL);
                    if (substr ($next_url, 0, strlen ($this->baseUrl)) != $this->baseUrl) {
                        continue;
                    }

                    if (!filter_var($next_url, FILTER_VALIDATE_URL)) {
                        continue;
                    }

                    foreach ($this->parseFilters as $filter) {
                        if (preg_match($filter, $next_url)) {
                            call_user_func_array($callback, ['url' => $next_url, 'content' => $html]);
                        }
                    }
                    foreach ($this->scanFilters as $filter) {
                        if (preg_match($filter, $next_url)) {
                            $this->scanned[] = $next_url;
                            break;
                        }
                    }
                }
                $this->addUrlToQueue($this->scanned);
            } catch (\Exception $e) {
                echo $url;
                echo "\n\n\n\n======================", $e->getMessage(), "======================\n\n\n\n";
            }
            $this->scanned = [];
        } while ($url = $this->getToScanUrl());
    }

    public function addUrlToQueue($urlsArray)
    {
        foreach ($urlsArray as $url) {
            $queue = new UrlQueue();
            $queue->url_source = $url;
            $queue->engine = $this->baseUrl;
            $queue->parsed = UrlQueue::TO_PARSE;
            UrlQueue::addToBatch($queue);
        }
        UrlQueue::saveBatch();
    }

    public function isScaned($url)
    {
        $queue = new UrlQueue();
        $queue->url_source = $url;
        $result = $queue->search(['url_source' => $url, 'category'=>$this->baseUrl, 'status'=>UrlQueue::TO_PARSE]);
    }

    public function clearQueue()
    {
        $queue = new UrlQueue();
        $queue->delete(['category' => $this->baseUrl]);
    }

    public function getToScanUrl()
    {
        $queue = new UrlQueue();
        $result = $queue->search(['category' => $this->baseUrl,'status' => UrlQueue::TO_PARSE]);
        if(!isset($result[0]) || !is_object($result[0])) {
            return false;
        }
        $queue->update(['status' => UrlQueue::PARSED],['id' => $result[0]->id]);
        return $result[0]->url_source;
    }


}