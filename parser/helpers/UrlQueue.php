<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 14.12.17
 * Time: 15:01
 */

namespace Parser\Helpers;


class UrlQueue extends BaseOriginModel
{

    const DS_ID = 4672;

    const TO_PARSE = 0;
    const PARSED = 1;

    public $url_source;
    public $parsed;
    public $engine;

    protected static $dsAddEnpointUrl = "http://ds.wiseweb.co/api/v1/origins/". self::DS_ID ."/records?api_key=" . DS_API_KEY;

    public function getDataToSend()
    {
        return [
            'url_source' => $this->url_source,
            'status' => $this->parsed,
            'category' => $this->engine,
        ];
    }
}