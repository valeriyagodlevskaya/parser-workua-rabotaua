<?php
/**
 * Created by PhpStorm.
 * User: lera
 * Date: 17.01.18
 * Time: 17:51
 */

namespace Parser\Helpers;

use Google_Client;
use Parser\Helpers\DataBaseModel;
use Google_Service_Sheets;
use Slim\PDO\Database;

/**
 * Class UserModel
 * @package Parser\WorkUa
 * @property Database $pdo
 */

class UserModel extends DataBaseModel
{
    public $phone, $email, $name, $city, $dateBirthday, $photo, $url, $description,
        $date, $external, $age, $phones;
    public $dayBirth;
    public $monthBirth;
    public $yerBirth;

    public static $columns = [
        'fullname',
        'phone',
        'city',
        'date_birthday',
        'photo',
        'url',
        'description',
        'date',
        'email',
        'external',
        'age',
        'phones'
    ];



    /**
     * @param $list
     * @return bool
     */
    public function save($list)
    {
        echo $this->external, ' '.$this->phone, "\n";

        if ($this->isUserExists($this->external, $this->email, $this->phone)) {
            return false;
        }
        $addData = $this->pdo->insert(static::$columns)
            ->into('candidate')
            ->values([
                $this->name,
                $this->phone,
                $this->city,
                $this->dateBirthday,
                $this->photo,
                $this->url,
                $this->description,
                $this->date,
                $this->email,
                $this->external,
                $this->age,
                $this->phones
            ]);
        $addData->execute(false);
        $this->addGoogle($list);

    }

    /**
     * @param $id
     * @param $email
     * @param $phone
     * @return int
     */
    public function isUserExists($id, $email, $phone)
    {
        var_dump($phone);
       $idUser =  $this->pdo->select()->from('candidate')
           ->where('external', '=', $id)
           ->orWhere('phone', '=', $phone)
           ->orWhere('email', '=', $email)
           ->execute();

        return $idUser->rowCount();

    }

    /**
     * Returns an authorized API client.
     * @return Google_Client the authorized client object
     */
    function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('parser-users');
        $client->setScopes(implode(' ', [
                Google_Service_Sheets::SPREADSHEETS]
        ));
        $client->setAuthConfig(__DIR__.'/../../api/key/client_secret_838568905552-tg3b02td8abocf46qn3dp2oek9m4em8e.apps.googleusercontent.com.json');
        //$client->setRedirectUri('http://parser.com/');
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');

        // Load previously authorized credentials from a file.
        $credentialsPath = __DIR__.'/../../api/credentials/sheets.googleapis.com-php-quickstart.json';
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));


            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if (!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    /**
     * @param $list
     */
    public function getFlag($list)
    {
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);
        $spreadsheetId = '1I8xHXE5ok9mdR3d1COjlDY6ckJ7oA3De5xzzdj3ACwA';
        //$range = $list.'!M3:M4';
        $g = new \Google_Service_Sheets_ValueRange();
        $g->setMajorDimension('ROWS');
        $g->getRange();
        $g->getValues();
        $ranges = [
            '!M3:M3'
        ];
        $params = array(
            'ranges' => $ranges
        );

        $result = $service->spreadsheets_values->batchGet($spreadsheetId, $params);

       // var_dump($result);die();

    }


    /**
     * @param $list
     */
    public function addGoogle($list)
    {
        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        // Prints the names and majors of students in a sample spreadsheet:
        // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
        $spreadsheetId = '1I8xHXE5ok9mdR3d1COjlDY6ckJ7oA3De5xzzdj3ACwA';
        $range = $list.'!A3:L3';
        $postBody = new \Google_Service_Sheets_ValueRange();
        $postBody->setMajorDimension('ROWS');
        $postBody->setRange($list.'!A3:L3');
        $photoAdd = '=IMAGE("'.$this->photo.'")';
        $postBody->setValues([
            [
                $this->date, $this->name, $this->email, '38'.$this->phone, $this->city, $this->age, $this->dayBirth,
                $this->monthBirth, $this->yerBirth, $photoAdd, $this->url, $this->description
            ]
        ]);

        $response = $service->spreadsheets_values->append($spreadsheetId, $range, $postBody, [
            "responseValueRenderOption"=> "FORMULA", "valueInputOption" => "USER_ENTERED"]);
    }



}