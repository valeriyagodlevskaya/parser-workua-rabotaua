<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 14.12.17
 * Time: 15:01
 */

namespace Parser\Helpers;


use GuzzleHttp\Client;

abstract class BaseOriginModel
{
    protected static $dsAddEnpointUrl;
    protected static $batchModels;

    protected static $bathLimit = 1000;

    public static function setApiAddEndpoint($dsId)
    {
        static::$dsAddEnpointUrl = "http://ds.wiseweb.co/api/v1/origins/". $dsId ."/records?api_key=" . DS_API_KEY;
    }

    public abstract function getDataToSend();

    public function save()
    {
        $client = new Client();
        $response = $client->request('POST', static::$dsAddEnpointUrl, [
            'json' => [$this->getDataToSend()],
        ]);
    }

    public function search($params, $limit = 1)
    {
        $client = new Client();
        $criteria = $item =[];
        foreach ($params as $key=>$param) {
            $item['column'] = $key;
            $item['type'] = 'equals';
            $item['value'] = $param;
            $criteria[] = $item;
        }
        if(!empty($criteria)){
            $response = $client->request('GET', static::$dsAddEnpointUrl, [
                'query' => ['api_key' => DS_API_KEY,'criteria' => $criteria, 'limit' => 1],
            ]);
        } else {
            $response = $client->request('GET', static::$dsAddEnpointUrl, [
                'query' => ['api_key' => DS_API_KEY, 'limit' => 1],
            ]);
        }
        return \GuzzleHttp\json_decode($response->getBody()->getContents());
    }

    public function delete($params = [])
    {
        $client = new Client();
        $criteria = $item =[];
        foreach ($params as $key=>$param) {
            $item['column'] = $key;
            $item['type'] = 'equals';
            $item['value'] = $param;
            $criteria[] = $item;
        }
        if(!empty($criteria)){
            $response = $client->request('DELETE', static::$dsAddEnpointUrl, [
                'json' => ['api_key' => DS_API_KEY,'criteria' => $criteria],
            ]);
        } else {
            $response = $client->request('DELETE', static::$dsAddEnpointUrl);
        }
        return $response->getBody()->getContents();
    }

    public function update(array $data, $params = [])
    {
        $client = new Client();
        $criteria = $item =[];
        foreach ($params as $key=>$param) {
            $item['column'] = $key;
            $item['type'] = 'equals';
            $item['value'] = $param;
            $criteria[] = $item;
        }
        if(!empty($criteria)){
            $response = $client->request('PATCH', static::$dsAddEnpointUrl, [
                'json' => [['data' => $data, 'criteria' => $criteria]],
            ]);
        } else {
            $response = $client->request('PATCH', static::$dsAddEnpointUrl, [
                'json' => [['data' => $data]],
            ]);
        }
        return $response->getBody()->getContents();
    }

    /**
     * @param string $model JsonModel
     */
    public static function addToBatch($model)
    {
        static::$batchModels[] = $model->getDataToSend();
        if(count(static::$batchModels)>static::$bathLimit) {
            static::saveBatch();
        }
    }


    public static function saveBatch()
    {
        $client = new Client();
        $response = $client->request('POST', static::$dsAddEnpointUrl, [
            'json' => static::$batchModels,
        ]);
        static::$batchModels = [];
    }
}