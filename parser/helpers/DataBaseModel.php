<?php
/**
 * Created by PhpStorm.
 * User: lera
 * Date: 17.01.18
 * Time: 17:07
 */

namespace Parser\Helpers;

use Slim\PDO\Database;

/**
 * Class DataBaseModel
 * @package Parser\Helpers
 * @property Database $pdo
 */
class DataBaseModel {

    protected $_pdo;

    public function connect()
    {
        $dsn = "mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
        $this->_pdo = new Database($dsn, DB_USERNAME, DB_PASSWORD);

    }
    public function __get($name)
    {
        $prName = '_'.$name;
        if (isset($this->$prName)) {
            return $this->$prName;
        }
        if (is_callable([$this, 'get'.ucfirst($name)])) {
            return call_user_func([$this, 'get'.ucfirst($name)]);
        }

    }

    public function getPdo()
    {
        if (!$this->_pdo) {
            $this->connect();
        }
        return $this->_pdo;
    }

}