// An example configuration file.
// exports.config = {
//     directConnect: true,
//
//     // Capabilities to be passed to the webdriver instance.
//     // capabilities: {
//     //     'browserName': 'chrome'
//     // },
//     multiCapabilities: [{
//         browserName: 'firefox'
//     }, {
//         browserName: 'chrome'
//     }]
//
//     // Framework to use. Jasmine is recommended.
//     //framework: 'jasmine',
//
//     // Spec patterns are relative to the current working directory when
//     // protractor is called.
//     specs: ['example_spec.js'],
//
//     // Options to be passed to Jasmine.
//     jasmineNodeOpts: {
//         defaultTimeoutInterval: 30000
//     }
//     seleniumAddress: 'http://0.0.0.0:4444/wd/hub',
// };
// An example configuration file.
exports.config = {
    directConnect: true,
    // browserstackUser: 'lera8',
    // browserstackKey: 'g7q5xn1x1EzqxHMTMHys',


    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        //'browserName': 'firefox',
        'browserName': 'chrome',
        'chromeOptions': {
            'args': ['--user-data-dir=/home/lera/chrome.txt']
        }
    },

    // Framework to use. Jasmine is recommended.
    framework: 'jasmine',

    // Spec patterns are relative to the current working directory when
    // protractor is called.
    specs: ['/home/lera/work/parser-work/api/telegram/test_spec.js'],

    // Options to be passed to Jasmine.
    jasmineNodeOpts: {
        defaultTimeoutInterval: 60000,
        includeStackTrace : true
    }
};
