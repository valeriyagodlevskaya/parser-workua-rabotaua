##Selenium  
Скачиваем Геккодрайвер    
`wget https://github.com/mozilla/geckodriver/releases/download/v0.18.0/geckodriver-v0.18.0-linux64.tar.gz`  
или ищем тут 
https://github.com/mozilla/geckodriver/releases

Распаковываем  
tar -xvzf geckodriver*  
Делаем исполняемым  
chmod +x geckodriver  
Копируем в bin  
sudo mv geckodriver /usr/local/bin/  
Или обавляем в переменные окружения   
export PATH=$PATH:/path-to-folder_where_extracted_geckodriver_binary_is

## Использование  
###Запуск selenium  
vendor/bin/selenium-server-standalone

###Запуск selenium  
xvfb-run vendor/bin/selenium-server-standalone


###Запустить selenium
cd ~/home/driver
\. start-chrome.sh; start-chrome 

Запуск protractor
protractor debug protractor.js

##Команды запуск парсера
php console.php work-ua:publisher --url='https://www.work.ua/resumes-kyiv-sales/?photo=1'


##Установить
apt-get install xvfb-run

###Установка geckodriver
wget https://github.com/mozilla/geckodriver/releases/download/v0.18.0/geckodriver-v0.18.0-linux64.tar.gz  

tar -xvzf geckodriver*  
chmod +x geckodriver  
export PATH=$PATH:/var/www/html/geckodriver  
mv geckodriver ~/usr/bin  

